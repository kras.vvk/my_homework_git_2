print("Задание 1__Работа со списком")
num_list = list(map(float, list(input("Введите список чисел через пробел =>").split())))

print(f"Min={min(num_list)}, Max={max(num_list)}, Сумма={sum(num_list)}, \
Средне арифметическое значение={round(sum(num_list) / len(num_list), 2)}")

print('-' * 30)

print("Задание 2__Работа со списком")
num_list_a = list(map(int, list(input("Введите список чисел через пробел первого ряда =>").split())))
num_list_b = list(map(int, list(input("Введите список чисел через пробел второго ряда =>").split())))
buf_list = []

for i in num_list_a:  # удаление совпадающих эл-в списка "a"
    if i not in buf_list:
        buf_list.append(i)
num_list_a = buf_list.copy()
buf_list.clear()
print("Удаление совпадающих эл-в первого списка", num_list_a)
for i in num_list_b:  # удаление совпадающих эл-в списка "b"
    if i not in buf_list:
        buf_list.append(i)
num_list_b = buf_list.copy()
buf_list.clear()
print("Удаление совпадающих эл-в второго списка", num_list_b)
for i in num_list_a:  # уникальные значения списка "a" относительно списка "b"
    if i not in num_list_b:
        buf_list.append(i)
print("Уникальные значения списка 'a' относительно списка 'b':", buf_list)
buf_list.clear()
for i in num_list_b:  # уникальные значения списка "b" относительно списка "a"
    if i not in num_list_a:
        buf_list.append(i)
print("Уникальные значения списка 'b' относительно списка 'a':", buf_list)
buf_list.clear()
for i in num_list_b:  # уникальные значения списка "a" и "b" (a+b)
    if i not in num_list_a:
        num_list_a.append(i)
print("У-е значения списка первого и второго (a+b) прямая послед-ть:", num_list_a)
print("У-е значения списка первого и второго (a+b) обратная послед-ть:", num_list_a[::-1])
print("У-е значения списка первого и второго (a+b) по возрастанию:", sorted(num_list_a))
print("У-е значения списка первого и второго (a+b) по убыванию:", sorted(num_list_a, reverse=True))

print('-' * 30)

print("Задание 3__Поиск простых чисел")
from math import prod

a = 0
b = 0
k = 0
ch = ' '
simple_nums = []
while a <= 1 or b <= 1:
    a = int(input("Введите начальное значение ряда =>"))
    b = int(input("Введите конечное значение ряда =>"))
    if a <= 1 or b <= 1:
        print("Промежуток для поиска простых чисел должен быть больше 1! Введите заново!")
    if a > b:
        a, b = b, a
for i in range(a, b + 1):
    for j in range(2, i):
        if i % j == 0:  # ищем количество делителей
            k += 1
    if k == 0:  # если делителей нет, добавляем число в список
        simple_nums.append(i)
    else:
        k = 0
print("Простые числа:", simple_nums)
print("""Для получения суммы ряда нажмите '1'
Для получения произведения ряда нажмите '2'
Не делать ни каких действий нажмите 'Enter'  """)
while ch:
    ch = input("Сделайте выбор =>").strip()
    print(ch)
    if ch == '1':
        print(f"Сумма ряда равна: {sum(simple_nums)}")
        break
    elif ch == '2':
        print(f"Произведения ряда равно: {prod(simple_nums)}")
        break

print('-' * 30)

print("Задание 4__Работа со списком")
i = 0
ch = ' '
list_1 = []
list_size = int(input("Введите размер списка =>"))
while i < list_size:
    el = input(f"Введите значение [{i}] =>").strip()
    list_1.append(el)
    i += 1
print("Введённый ряд:", list_1)
print("""Для отображения списка в обратном порядке нажмите '1'
Для отображения списка в порядке возрастания нажмите '2'
Не делать ни каких действий и выйти нажмите 'Enter' """)
while ch:
    ch = input("Сделайте выбор =>").strip()
    print(ch)
    if ch == '1':
        print(f"В обратном порядке: {list_1[::-1]}")
    elif ch == '2':
        print(f"В порядке возрастания: {sorted(list_1)}") # сортировка работает по модулю
        # (не работает с отрицательными числами)

print('-' * 30)

print("Задание 5__Работа со списком. Натуральные числа.")
ch = ' '
int_list = []
new_list = []
while ch:
    ch = input("Введите число ('Enter' - заверешения ввода) =>")
    if ch.isdigit() and int(ch) > 0:
        int_list.append(int(ch))
        if int(ch) % 2 != 0:
            new_list.append(int(ch))
    elif not ch.isdigit() or int(ch) == 0:
        if ch:
            print("Вы ввели не цифру или не натуральное число!")
print(f"Список int_list: {int_list}")
print(f"Очищенный список int_list {int_list.clear()}")
print(f"Список new_list: {new_list}")
repeat = int(input("Введите количество дублирования списка new_list =>"))
new_list = new_list.copy() * repeat
print(f"Новый список new_list с повторами: {new_list}")

print('-' * 30)

print("Задание 6__Работа со списком. Кол-во повторов и позиция числа.")
num = int(input("Введите число для проверки =>"))
indexes_num = []
if num in new_list:
    new_list.count(num)
    for i in range(0, len(new_list)):
        if new_list[i] == num:
            indexes_num.append(i)
    print(f"Ко-во повторов числа {num} в списке new_list =  {new_list.count(num)}")
    print(f"Индексы искомого элемента ({num}) -> {indexes_num}")
else:
    print(f"Введенное число {num} не найденно в списке new_list!!!")
print('-' * 30)
