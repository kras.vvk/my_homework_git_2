print("Задание 1__Функция приветсвия")


def welcome_fun(hello="Valentyn"):
    print(f"Welcome {hello}!!!")


user_name = input("Введите своё имя =>").strip()
if user_name == '':
    welcome_fun()
else:
    welcome_fun(user_name)

print('-' * 30)

print("Задание 2__Алгебраические выражения (3 * x1 + 2) и (x1 ** 2) ")
from numpy import arange


def expression_1(x1):
    return 3 * x1 + 2


def expression_2(x1):
    return x1 ** 2


for x in arange(-5, 5.5, 0.5):
    print(f"Выражение_1 -> x({x})={expression_1(x)}  Выражение_2 -> x({x})={expression_2(x)}")

print('-' * 30)

print("Задание 3__Калькулятор")
operation_ch = [0]
a = 0
b = 0


def sum(a, b):
    return print(f"Сумма {a + b}")


def subtraction(a, b):
    return print(f"Разность {a - b}")


def multiplication(a, b):
    return print(f"Произведение {a * b}")


def division(a, b):
    if b == 0:
        return print("Делить на ноль нельзя!!!")
    else:
        return print(f"Деление {a / b}")


def exponentiation(a, b):
    return print(f"Возведения в степень {a ** b}")


def square_root(a):
    return print(f"Корень квадратный {a ** 0.5}")


def cube_root(a):
    return print(f"Корень кубический {a ** (1. / 3.)}")


print("""Калькулятор поддерживает следующие операции:
-сложение (+)\n-вычитание (-)\n-умножение (*)\n-деление (/)\n-возведения в степень (**)
-корень квадратный (sqr)\n-корень кубический (cr)\n-завершить работу - 'Enter'""")
print("Введите данные через пробел (Пример: 2 + 2, 3 sqr и т.д.)")

while len(operation_ch) != 0:
    operation_ch = list(input("=>").split())
    if len(operation_ch) == 0:
        break
    if len(operation_ch) == 2:
        operation_ch[1] = operation_ch[1].lower()
        if operation_ch[1] != 'sqr' and operation_ch[1] != 'cr':
            print("Информация введена не корректно!!!")
            continue
        a = float(operation_ch[0])
    elif len(operation_ch) == 3:
        a = float(operation_ch[0])
        b = float(operation_ch[2])
    else:
        print("Информация введена не корректно!!!")
        continue
    operation_number = operation_ch[1]
    match operation_number:
        case '+':
            sum(a, b)
        case '-':
            subtraction(a, b)
        case '*':
            multiplication(a, b)
        case '/':
            division(a, b)
        case '**':
            exponentiation(a, b)
        case 'sqr':
            square_root(a)
        case 'cr':
            cube_root(a)
        case _:
            print("Операция не известна! Информация введена не корректно!!!")

print('-' * 30)

print("Задание 4__Вычисление среднего арифметического трёх чисел")
num_list = [0]


def average_fun(num_list):
    avg = (num_list[0] + num_list[1] + num_list[2]) / 3
    return print(f"Средне арифметическое введеных чисел {avg} ")


while len(num_list) != 0:
    num_list = list(map(float, list(input("Введите три числа через пробел =>").split())))

    if len(num_list) < 3 and len(num_list) != 0:
        print("Введено не достаточное количество данных. Повторите ввод!!!")
        continue
    if len(num_list) > 3:
        print("Введено слишком много значений. Повторите ввод!!!")
        continue
    elif len(num_list) == 3:
        average_fun(num_list)
        print("Для завершения программы нажмите 'Enter'")

print('-' * 30)

print("Задание 5__Анализ массы тела")
ch = ''


def body_mass_index(height, weight):
    bmi = weight / (height ** 2)
    if bmi < 18.5:
        return print(f"ИМТ={format(bmi, '.2f')} --> Недостаточный вес!")
    elif bmi > 25:
        return print(f"ИМТ={format(bmi, '.2f')} --> Cледите за фигурой!")
    return print(f"ИМТ={format(bmi, '.2f')} --> Масса тела в норме!")


while ch != 'off':
    ch = input("Введите рост =>")
    if ch.lower() == 'off':
        break
    else:
        height = float(ch)
    ch = input("Введите вес =>")
    if ch.lower() == 'off':
        break
    else:
        weight = float(ch)

    body_mass_index(height, weight)
    print("Для завершения программы введите 'off'")

print('-' * 30)
