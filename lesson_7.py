print("Задание 1__Пересечение множеств")
set_1 = set(input("Введите первый ряд эл-ов через пробел =>").split())
set_2 = set(input("Введите второй ряд эл-ов через пробел =>").split())

print("Первый ряд: ", set_1)
print("Второй ряд:", set_2)
print("Элементы, которые есть в обеих рядах: ", set_1 & set_2)

print('-' * 30)

print("Задание 2__Эмулятор сервиса по сокращению ссылок")
ch = ' '
link_dict = {}

print("Добавление ссылки и короткого названия:")
long_link = input("Введите ссылку =>")
short_name = input("Введите сокрощенное название ссылки =>")
link_dict[short_name] = long_link

print("\nМеню:\n"
      "Вывести все добавленные ссылки и их сокращения  - 1\n"
      "Найти введенную ссылку и сокрощенное название - 2\n"
      "Добавить ссылку и сокращенное название - 3\n"
      "Удалить ссылку и сокращенное название - 4\n"
      "Закончить работу с программой - 'Enter'\n")
while ch:
    operation_number = input("Действия с ссылками, номер от 1 до 4 =>").strip()
    match operation_number:
        case '1':
            print("---Вывести все добавленные ссылки и их сокращения---")
            for short_name, long_link in link_dict.items():
                print(f"Короткое название ссылки: {short_name} --> Ссылка: {link_dict[short_name]}")

        case '2':
            print("---Найти введенную ссылку и сокрощенное название---")
            short_name = input("Введите сокрощенное название ссылки =>")
            if short_name in link_dict:
                print("Ссылка найдена!!!")
                print(f"Короткое название ссылки: {short_name} --> Ссылка: {link_dict[short_name]}")
            else:
                print("Такого короткого название ссылки не найдено !")

        case '3':
            print("---Добавить ссылку и сокращенное название---")
            long_link = input("Введите ссылку =>")
            short_name = input("Введите сокрощенное название ссылки =>")
            link_dict[short_name] = long_link

        case '4':
            print("---Удалить ссылку и сокращенное название---")
            short_name = input("Введите сокрощенное название ссылки =>")
            if short_name in link_dict:
                del link_dict[short_name]
                print("Ссылка удалена!!!")
            else:
                print("Такого короткого название ссылки не найдено !")

        case '':
            ch = ''

print('-' * 30)

print("Задание 3__Уникальные списки")
list_1 = list(map(int, list(input("Введите первый ряд чисел через пробел =>").split())))
list_2 = list(map(int, list(input("Введите второй ряд чисел через пробел =>").split())))

print("Первый список чисел: ", list_1)
print("Второй список чисел:", list_2)
print("Уникальные значения 1-го списка относительно 2-го: ", set(list_1) - set(list_2))
print("Уникальные значения 2-го списка относительно 1-го: ", set(list_2) - set(list_1))

print('-' * 30)

print("Задание 5__Словарь с подсчетом уникальных слов")
print("Введите строку, которая будет повторено столько раз, что бы кол-во слов было не менее 1000!")
dict_word_key = {}
str_text = input("Введите строку =>")

while len(list(str_text.split())) < 1000:
    str_text = (str_text + ' ') * 2

for word in str_text.split():
    dict_word_key[word] = dict_word_key.get(word, 0) + 1

# for word in str_text.split(): # ===== 2й вариант решения
#     if word in dict_word_key:
#         dict_word_key[word] += 1
#     else:
#         dict_word_key[word] = 1

print(f"Словарь с уникальными значениями: {dict_word_key}")
print(f"Кол-во слов в введенной строке: {len(list(str_text.split()))}\n"
      f"Кол-во уникальных слов: {len(dict_word_key)}\n"
      )

print('-' * 30)

print("Задание 6__Прототип программы 'Библиотека'")
ch = ' '
labre_dict = {'Эрик Мэтиз': 'Изучаем Python',
              'Пол Бэрри': 'Изучаем программирование на Python',
              'Марк Лутц': 'Изучаем Python',
              'Билл Любанович': 'Простой Python',
              'Дэн Бейдер': 'Чистый Python',
              'Лучано Рамальо': 'Python',
              'Франсуа Шолле': 'Глубокое обучение на Python',
              }

for author, name in labre_dict.items():
    print(f"Автор: {author} -> Название книги: {name}")
print("\nМеню:\n"
      "Сортирока по автору - 1\n"
      "Сортировка по названию произведения - 2\n"
      "Добавить книгу - 3\n"
      "Удалить книгу - 4\n"
      "Закончить работу с библиотекой - 'Enter'\n")
while ch:
    operation_number = input("Действия с библиотекой, номер от 1 до 4 =>").strip()
    match operation_number:
        case '1':
            print("---Сортирока по автору---")
            for author, name in sorted(labre_dict.items()):
                print(f"Автор: {author} -> Название книги: {name}")

        case '2':
            print("---Сортировка по названию произведения---")
            for author, name in sorted(labre_dict.items(), key=lambda para: (para[1], para[0])):
                print(f"Автор: {author} -> Название книги: {name}")

        case '3':
            print("---Добавить книгу---")
            author_book = input("Введите автора книги =>")
            name_book = input("Введите название книги =>")
            labre_dict[author_book] = name_book

        case '4':
            print("---Удалить книгу---")
            author_book = input("Введите автора книги =>")
            if author_book in labre_dict:
                del labre_dict[author_book]
                print("Книга удалена!!!")
            else:
                print("Такой книги в библиотеке нет!")

        case '':
            ch = ''

print('-' * 30)

print("Задание 7__Прототип программы 'Учёт кадров'")
ch = ' '
dict_spec = {}  # словарь используеться для добавления нового работника
dict_ef = {}  # словарь используеться для вывода данных отсортированных по эффективности работников
sort_ef_list = []  # список используеться для вывода данных отсортированных по эффективности работников

workers_dict = {
    'Петров': {
        'Должность': 'разработчик',
        "Опыт работы": 2.5,
        'Портфолио': "SQL, WordPress",
        "Коэффициент эффективности": 75,
        "Стек технологий": "языки программирования, фрэймворки, компиляторы",
        'Зарплата': 1500
    },
    'Сидоров': {
        'Должность': 'старший разработчик',
        "Опыт работы": 3,
        'Портфолио': "Python, SQL, Java",
        "Коэффициент эффективности": 51,
        "Стек технологий": "языки программирования, СУБД",
        'Зарплата': 2000
    },
    'Иванов': {
        'Должность': 'junior разработчик',
        "Опыт работы": 0.7,
        'Портфолио': "SQL, WordPress, Java",
        "Коэффициент эффективности": 80,
        "Стек технологий": "языки программирования, фрэймворки",
        'Зарплата': 1200
    }
}
print("\nМеню:\n"
      "Вывести весь список работников  - 1\n"
      "Вывести отсортированный по имени список работников - 2\n"
      "Вывести отсортированный по эффективности список работников - 3\n"
      "Добавить работника - 4\n"
      "Удалить работника - 5\n"
      "Закончить работу с учетом кадров - 'Enter'")
while ch:
    operation_number = input("\nДействия с учетом кадров, номер от 1 до 5 =>").strip()
    match operation_number:
        case '1':
            print("---Вывести весь список работников---")
            for name_worker, workers_data in workers_dict.items():
                print(f" \nФамилия работника: {name_worker}")
                for data, workers_spec in workers_data.items():
                    print(f"{data}: {workers_spec}")

        case '2':
            print("---Вывести отсортированный по имени список работников---")
            for name_worker, workers_data in sorted(workers_dict.items()):
                print(f" \nФамилия работника: {name_worker}")
                for data, workers_spec in workers_data.items():
                    print(f"{data}: {workers_spec}")

        case '3':
            print("---Вывести отсортированный по эффективности список работников---")
            dict_ef = {}
            sort_ef_list = []
            for name_worker, workers_data in workers_dict.items():
                for data, workers_spec in workers_data.items():
                    if data == "Коэффициент эффективности":
                        dict_ef[name_worker] = workers_spec  # Получения словаря Фамилия -> к-т эффек-ти
            for name_worker, workers_data in sorted(dict_ef.items(), key=lambda para: (para[1], para[0])):
                sort_ef_list.append(name_worker)  # Получения отсортир-ного списка Фамилий по эффек-ти

            for name_worker in sort_ef_list:
                print(f" \nФамилия работника: {name_worker}")
                for data, workers_spec in workers_dict[name_worker].items():
                    print(f"{data}: {workers_spec}")

        case '4':
            print("---Добавить работника---")

            name_worker = input("Введите фамилию работника =>")
            dict_spec['Должность'] = input("Введите должность работника =>")
            dict_spec["Опыт работы"] = float(input("Введите опыт работника (цифра) =>"))
            dict_spec['Портфолио'] = input("Введите портфолио работника =>")
            dict_spec["Коэффициент эффективности"] = int(
                input("Введите коэффициент эффективности работника (цифра) =>"))
            dict_spec["Стек технологий"] = input("Введите стек технологий работника =>")
            dict_spec['Зарплата'] = int(input("Введите зарплату работника (цифра) =>"))

            workers_dict[name_worker] = dict_spec

        case '5':
            print("---Удалить работника---")
            name_worker = input("Введите фамилию работника =>")
            if name_worker in workers_dict:
                del workers_dict[name_worker]
                print("Работник удалён!!!")
            else:
                print("Такого работника нет!")

        case '':
            ch = ''

print('-' * 30)
