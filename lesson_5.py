print("Задание 1__Проверка ФИО")
fio = input("Введите ФИО =>")
print(fio)
for s in fio.split():
    if not s.isalpha() or not s.istitle():
        print("Проблемма!!! ФИО содержит не только буквы или ФИО введено не корректно!  ")
        break
else:
    print(f"Вы ввели: {fio} Проверка прошла успешно! ")

print('-' * 30)

print("""Задание 2__Арифметические действия с последовательностью (не менее 5 чисел).
Для завершения ввода нажмите 'Enter'.""")
ch = ' '
numbers = []
while ch:
    ch = input("Введите число для внесения в числовой ряд =>")
    if ch.isdigit():
        numbers.append(float(ch))
    elif not ch.isdigit() and len(numbers) < 5:
        print("Вы ввели не цифру или кол-во элементов ряда меньше 5!")
        ch = ' '

print(f"Второй эл-т = {numbers[1]}, предпоследний = {numbers[len(numbers) - 2]}, \
среднее арифметическое ряда = {round(sum(numbers) / len(numbers), 2)}, их сумма = \
{numbers[1] + numbers[len(numbers) - 2] + round(sum(numbers) / len(numbers), 2)} ")

print('-' * 30)

print("Задание 3__RGB")
flag = False
while not flag:
    r, g, b = map(int, input("Введите цвет RGB (через пробел) =>").split())
    flag = True
    if r < 0 or r > 255:
        print("Значение 'R' не в диапозоне от 0 до 255!")
        flag = False
    elif g < 0 or g > 255:
        print("Значение 'G' не в диапозоне от 0 до 255!")
        flag = False
    elif b < 0 or b > 255:
        print("Значение 'B' не в диапозоне от 0 до 255!")
        flag = False
    else:
        t = r, g, b
        print(t, type(t))
        flag = True

print('-' * 30)

print("Задание 4__Создание фабрики именованных кортежей оценок учеников.")
from collections import namedtuple

Student = namedtuple('Student', 'name algebra geometry history computer_science geography')
students = (
    Student('Иванов', 7, 8, 9, 10, 12),
    Student('Петров', 7, 4, 10, 11, 6),
    Student('Сидоров', 8, 5, 10, 11, 12),
    Student('Макаренко', 4, 8, 9, 10, 11),
    Student('Шевченко', 11, 9, 10, 11, 8),
    Student('Усик', 11, 8, 9, 10, 12),
    Student('Ломаченко', 7, 11, 9, 6, 9),
)
for student in students:
    print(student)

print('-' * 30)

print("""Задание 5__Сортировка последовательности чисел.\n
Для завершения ввода нажмите 'Enter'.""")
ch = ' '
numbers = []
while ch:
    ch = input("Введите число для внесения в числовой ряд =>").strip()
    if ch.isdigit():
        numbers.append(int(ch))
    elif not ch.isdigit() and ch != "":
        print("Вы ввели не цифру!")
        continue
numbers.sort()
numbers = tuple(numbers)
print(numbers)

print('-' * 30)

print("Задание 6__Анализ фидбэка.")
review = input("Напишите отзыв =>")
bonus = 0
flag = [True, True, True]
for word in review:
    if 'меню' in review and flag[0] == True:
        bonus += 5
        flag[0] = False
    elif 'спортзал' in review and flag[1] == True:
        bonus += 5
        flag[1] = False
    elif 'обслуживание' in review and flag[2] == True:
        bonus += 5
        flag[2] = False
if len(review) > 60 and bonus > 0:
    print(f"Вы получили дополнительно бонус {bonus}%!")

print('-' * 30)
